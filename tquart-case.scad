// case for https://github.com/thequux/usb-uart

$fa = 3.5;
$fs = 0.15;

include <oshw.scad>;

// values measured and gathered from KiCad

board_clearance = 0.35;

switch_x = 12.7 + board_clearance * 2;
switch_y = 04.5 + board_clearance;
switch_z = 04.8 + board_clearance;
switch_pins_x = switch_x;
switch_pins_y = 1.5;
switch_pins_z = 2.90;
switch_toggle_x = switch_x - (2 * 3.5);
switch_toggle_y = 4;
switch_toggle_z = 2 + board_clearance;

header_h = 9; // actually measured 8.6, using this to fudge some alignment

board_x = 85.45;
board_y = 33.40;
board_z = 01.66;

usb_x = 6;
usb_y = 8;
usb_x_overhang = -1;
usb_z_underhang = -0.5;
usb_z = 3 + usb_z_underhang + board_clearance + 0.25 + 0.3;

case_radius = 2;
case_padding = 12;
case_splitline = 4.25;
case_top = board_z + header_h; // where the top of the case should be positioned
case_z = board_z + case_padding; // height of the case
case_z_p = case_top - (case_z / 2); // align top of case with top of header
case_bottom = (case_z_p - (case_z / 2));
text_depth = 0.2;
font="D\\-DIN";
logo_depth = 0.2;

mount_off_x = 4.844;
mount_off_y = 4.250;
mount_x = 76;
mount_y = 25;

module header (pins=1) {
	length = 2.54 * 3;
	width = 2.54 * pins;
	translate([0, length, 0])
	cube([length, width, 2.54]);
};

module sw_sp3t() {
	// body
	color("silver")
	cube([switch_x, switch_y, switch_z], center=true);

	// pins
	translate([0, 0, -((switch_z/2) + (switch_pins_z/2))])
	color("silver")
	cube([switch_pins_x, switch_pins_y, switch_pins_z], center=true);

	// toggle over travel
	translate([0, -switch_y + 0.5, 0])
	color("black")
	cube([switch_toggle_x, switch_toggle_y, switch_toggle_z], center=true);
};

module sp3t_access() {
	// access to the toggle
	translate([0, -7, 0])
	cube([switch_x + 2, switch_y + 2, switch_z + 2], center=true);
};

module usb_micro_b() {
	rear_clearance = 1;
	color("grey")
	translate([(usb_x/2) + (rear_clearance/2) + usb_x_overhang, 0, (usb_z/2) + usb_z_underhang])
	cube([usb_x + rear_clearance, usb_y, usb_z + usb_z_underhang + board_clearance], center=true);
};

module usb_access() {
	// access to the USB port
	translate([usb_x/2, 0, usb_z/2 + board_z])
	translate([usb_x_overhang - board_clearance * 2, 0, usb_z_underhang]) {
		translate([-((8 + usb_x) / 2), 0, -1.5])
		cube([8, 13, 12.5], center=true);
	};
};

module corner_reliefs() {
	translate([0, 0, 0])
	cylinder(r=0.2, h=board_z);
	translate([board_x, 0, 0])
	cylinder(r=0.2, h=board_z);
	translate([0, board_y, 0])
	cylinder(r=0.2, h=board_z);
	translate([board_x, board_y, 0])
	cylinder(r=0.2, h=board_z);
};

module board_clearance(access=true) {
	// access bool enables the subtracted blocks to access the USB port and voltage switch
	// these need to be disabled in board_path to not have a mismatch in their sizes
	// as board_path adds clearance around the entire model

	translate([0, 0, board_z]) {

		// cube around board to allow clearance and remove the mounting holes
		translate([0, -(board_y / 2), -board_z])
		cube([board_x, board_y, board_z]);

		// USB port
		if (access) {
			translate([0, 0, -board_z])
			usb_access();
		};

		if (access) {
			translate([15.971 + 1.42, -14.859, switch_z/2 - board_clearance])
			sp3t_access();
		};

		// power input header
		translate([38.2 + 7.775 + 2.54, -((2.54 * 3) + board_clearance + board_y / 2), 0.05])
		rotate([0, 0, 90])
		header(2);

		// power input header body and pins
		translate([38.2 - 2.386, -(board_clearance + board_y / 2), -3.2])
		cube([2.54 * 2, 5, 5.8]);

		// output header
		translate([board_x - 0.478, -((2.54 * 6) - 0.548), 0.05])
		header(6);

		// selection jumpers
		translate([60.73, -3.30, 2.59])
		rotate([0, 0, 90])
		minkowski() {
			union() {
				header(4);
				translate([0, 0, 2.54])
				header(4);
				translate([0, 0, 2.54 * 2])
				header(4);
			};
			cube(size=(2*board_clearance)+0.30, center=true);
		};

		// crossover jumpers
		translate([55.372, -9.65, (2.54 * 3) + 2.59])
		rotate([0, 90, 0])
		minkowski() {
			union() {
				header(2);
				translate([0, 0, 2.54])
				header(2);
			};
			cube(size=(2*board_clearance), center=true);
		};

		// box header
		translate([68.961, 0.525, 11 / 2])
		cube([9.2 + board_clearance, (2.54 * 3) + 7.98 + board_clearance, 11], center=true);

		// most of the header bases and pins
		translate([board_x - (43 / 2), 0, -0.25])
		cube([43, 17, 5.7], center=true);

		// SMD components
		translate([8, -13, 0])
		cube([32.89, 29.75, 2.6]);

		translate([0, -(board_y/2), -board_z])
		corner_reliefs();

		usb_micro_b();
		translate([-(usb_x/2), 0, 0])
		usb_micro_b();
	};
};

module populated_board() {
	// raise entire assembly to allow working relative to board top within this block
	translate([0, 0, board_z]) {
		// bare PCB exported from KiCad as STEP and converted to STL with FreeCAD
		color("green")
		//translate([board_x/2, 0, -board_z])
		//import("tc2030-ch430.stl");
		translate([0, -board_y/2, -board_z])
		cube([board_x, board_y, board_z]);

		// USB port
		usb_micro_b();

		translate([15.971 + 1.42, -14.859, switch_z/2])
		sw_sp3t();
	};
};

module screw(insert=false) {
	M3_head = 5.5;
	M3_drill = 2.5;
	M3_clearance = 3.2;
	countersink = M3_head + board_clearance;

	SL_insert_l = 4;
	SL_insert_hole = 4.3;

	// unused, clearance "above" the screw-head
	translate([0, 0, -1])
	cylinder(d = countersink, h = 1);

	// clearance for bottom half
	cylinder(d = M3_clearance, h = case_bottom + board_z);

	// countersink head
	cylinder(r1 = (countersink / 2), r2 = 0, h = (countersink / 2));
	cylinder(d = M3_drill, h = 12);
	if (insert) {
		// clearance behind the insert
		cylinder(d = M3_clearance, h = 12);

		translate([0, 0, -(case_bottom) + board_z])
		cylinder(d = SL_insert_hole, h = SL_insert_l);
	};
};

module screws(inserts=false) {
	translate([mount_off_x, -(board_y / 2) + mount_off_y, case_bottom]) {
		screw(insert=inserts);
		translate([mount_x, 0, 0])
		screw(insert=inserts);
		translate([0, mount_y, 0])
		screw(insert=inserts);
		translate([mount_x, mount_y, 0])
		screw(insert=inserts);
	};
};

module labeling_top() {
	translate([0, 0, case_top])
	rotate([0, 0, -90]) {
		// height must be positive, i want it negative, but also visible, so not just offsetting it to align with the case
		translate([0, 0, -text_depth])
		linear_extrude(height=text_depth) {
			text("USB", valign="bottom", halign="center", font=font, size=6);
			translate([board_y / 2, 11, 0]) {
				padding=6.5;
				text("5V", valign="center", halign="right", font=font, size=5);
				translate([0, padding, 0])
				text("Ext.", valign="center", halign="right", font=font, size=5);
				translate([0, padding * 2, 0])
				text("3V3", valign="center", halign="right", font=font, size=5);
			};
			translate([(board_y / 2) + 2, 31.5, 0]) {
				text("Ext. In", valign="center", halign="right", font=font, size=4.2);
				translate([0, 4.5, 0])
				text("+", valign="center", halign="right", font=font, size=5);
				translate([0, 8, 0])
				text("-", valign="center", halign="right", font=font, size=5);
			};
			translate([-(board_y / 2), 69, 0])
			text("A", valign="center", halign="left", font=font, size=7);
			translate([-6, 40, 0]) {
				padding=3;
				text("DSR", valign="center", halign="right", font=font, size=5);
				translate([0, (2.54 + padding), 0])
				text("CTS", valign="center", halign="right", font=font, size=5);
				translate([0, (2.54 + padding) * 2, 0])
				text("DTR", valign="center", halign="right", font=font, size=5);
				translate([0, (2.54 + padding) * 3, 0])
				text("RTS", valign="center", halign="right", font=font, size=5);
			};

			translate([(board_y / 2), 69, 0])
			text("B", valign="center", halign="right", font=font, size=7);
			*translate([5, 45, 0]) {
				padding=0.86;
				text("DSR", valign="center", halign="left", font=font, size=3);
				translate([0, (2.54 + padding), 0])
				text("CTS", valign="center", halign="left", font=font, size=3);
				translate([0, (2.54 + padding) * 2, 0])
				text("DTR", valign="center", halign="left", font=font, size=3);
				translate([0, (2.54 + padding) * 3, 0])
				text("RTS", valign="center", halign="left", font=font, size=3);
			};

			rotate([0, 0, 90])
			translate([63, -7, 0]) {
				translate([-3.5, 0, 0])
				text("normal ||", valign="center", halign="right", font=font, size=4.1);
				translate([0, -6, 0])
				text("swapped =", valign="center", halign="right", font=font, size=4.1);
			};

			padding=3.3;
			translate([(2.54 + padding * 3) + 2, board_x + 2, 0])
			rotate([0, 0, 90]){
				text("Vtgt", valign="center", halign="right", font=font, size=5);
				translate([0, (2.54 + padding), 0])
				text("B", valign="center", halign="right", font=font, size=5);
				translate([0, (2.54 + padding) * 2, 0])
				text("RXD", valign="center", halign="right", font=font, size=5);
				translate([0, (2.54 + padding) * 3, 0])
				text("TXD", valign="center", halign="right", font=font, size=5);
				translate([0, (2.54 + padding) * 4, 0])
				text("GND", valign="center", halign="right", font=font, size=5);
				translate([0, (2.54 + padding) * 5, 0])
				text("A", valign="center", halign="right", font=font, size=5);
			};

		};

		// separate logo to adjust depth
		translate([-5, 27, -logo_depth])
		linear_extrude(height=logo_depth)
		oshw_logo_2d(15);
	};
};

module labeling_bottom() {
	translate([board_x, 0, case_z_p - (case_z / 2)])
	mirror([1, 0, 0]) {
		// height must be positive, i want it negative, but also visible, so not just offsetting it to align with the case
		linear_extrude(height=text_depth) {
			translate([board_x/2, (board_y/2) - mount_off_y, 0])
			text("https://hackerspace.gent", valign="center", halign="center", font=font, size=4.5);

			translate([(board_x/2) - 1, 0, 0])
			text("gitlab.com/evils/usb-uart-case", valign="center", halign="center", font=font, size=4.8);

			translate([board_x/2, -((board_y/2) - mount_off_y), 0])
			text("CERN-OHL-W", valign="center", halign="center", font=font, size=5);
		};
	};
};


// case minus board and clearances
// the board sits on Z 0, everything else is positioned around it
module case_full(label=false, inserts=true) {
	difference() {
		translate([board_x / 2, 0, case_z_p])
		minkowski() {
			cube([board_x + case_padding - (case_radius * 2),
                              board_y + case_padding - (case_radius * 2),
                              case_z - (case_radius * 2)],
                              center = true);
			sphere(case_radius + 0.16, $fn = 8);
		};

		populated_board();
		board_clearance();

		// mounting holes
		screws(inserts=inserts);

		// text
		if (label) {
			labeling_top();
			labeling_bottom();
		};
	};
};

module board_path(clearance=0, access=false) {
	// remove upwards path of board and ports
	minkowski() {
		board_clearance(access=access);
		translate([0, 0, 5/2])
		// basically a line if given clearance is 0
		cube([0.001 + (clearance * 2), 0.001 + (clearance * 2), 5], center=true);
	};
};

module case_bottom(label=false) {
	difference() {
		case_full();

		// top half
		translate([-(board_x / 2), -((board_y / 2) + 10), case_splitline])
		cube([board_x * 2, board_y * 2, 100]);

		board_path(clearance=board_clearance, access=false);

		if (label) labeling_bottom();
	};
};

module case_bottom_no_clearance() {
	difference() {
		case_full();

		// top half
		translate([-(board_x / 2), -((board_y / 2) + 10), case_splitline])
		cube([board_x * 2, board_y * 2, 100]);

		board_path(0);
	};
};

module case_top(label=false, inserts=true) {
	difference() {
		case_full(inserts=inserts);

		// bottom half minus a block that extends down to the board
		difference() {
			translate([-(board_x / 2), -((board_y / 2) + 10), - 100 + case_splitline])
			cube([board_x * 2, board_y * 2, 100]);
			translate([board_x / 2, 0, case_splitline])
			cube([board_x, board_y, case_splitline], center=true);
		};

		if (label) labeling_top();
	};
};

module case_top_2(label=false, inserts=true) {
	difference() {
		case_full(label=label, inserts=true);

		case_bottom_no_clearance();
	};
};

translate ([0, board_y / 2, 0]) {
	//translate([0, 0, 0.1])
	//rotate([180, 0, 0])
	//case_top();
	//translate([0, board_y * 1.5, 0])
	//rotate([180, 0, 0])
	//case_top_2();
	//case_bottom_no_clearance();
	//case_bottom();
	//case_full();
	//screws(inserts=true);

	//populated_board();
	//board_path(board_clearance);
	//board_clearance(access=true);
	//usb_access();
};
