include <tquart-case.scad>;

part = "undef";
label = false;
inserts = true;

module top() {

	translate([case_padding - case_radius, (board_y / 2) + case_padding - case_radius, case_top])
	rotate([180, 0, 0])
	case_top(label=label, inserts=inserts);
};

module bottom() {

	translate([case_padding - case_radius, (board_y / 2) + case_padding - case_radius, case_bottom])
	case_bottom(label=label);
};

module full() {

	color("grey")
	translate([case_padding - case_radius, (board_y / 2) + case_padding - case_radius, case_bottom])
	case_full(label=label, inserts=inserts);
};

module exploded() {

	translate([0, 0, 10])
	case_top(label=label, inserts=inserts);
	case_bottom(label=label);
};

module top_labeling() {

	translate([case_padding - case_radius, (board_y / 2) + case_padding - case_radius, case_top])
	rotate([180, 0, 0])
	labeling_top();
};

module bottom_labeling() {

	translate([case_padding - case_radius, (board_y / 2) + case_padding - case_radius, case_bottom])
	labeling_bottom();
};

if (part == "top") top();
if (part == "bottom") bottom();
if (part == "full") full();
if (part == "exploded") exploded();
if (part == "top_labeling") top_labeling();
if (part == "bottom_labeling") bottom_labeling();
