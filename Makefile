MAKEFLAGS := -j
OUT = rendered

LABEL = false
INSERTS = true
RADIUS = 3

.PHONY: top
top: $(OUT)/top.stl

.PHONY: bottom
bottom: $(OUT)/bottom.stl

.PHONY: full
full: $(OUT)/full.stl

.PHONY: exploded
exploded: $(OUT)/exploded.stl

.PHONY: top_labeling
top_labeling: $(OUT)/top_labeling.stl

.PHONY: bottom_labeling
bottom_labeling: $(OUT)/bottom_labeling.stl

.PHONY: labeling
labeling: top_labeling bottom_labeling

.PHONY: print
print: top bottom labeling

.PHONY: all
all: print full exploded top_labeling bottom_labeling

$(OUT)/%.stl: case.scad
	@mkdir -p $(@D)
	openscad -Dpart=\"$(*F)\" -Dlabel=$(LABEL) -Dinserts=$(INSERTS) -Dcase_radius=$(RADIUS) -o $@ $<

.PHONY: clean
clean:
	rm -f $(OUT)/*.stl
